# Sortan Application

Sortan application allows users to recognize Khmer songs in any occassions.

# Installation

Sortan application is built using **Flutter** framework. During the implementation, we use **Visual Studio** code as the IDE. 

Therefore, you should first install Flutter framework into your computer. Then you should intall Visual Studio code. However, you could also use other IDEs as well.

If you want to use Visual Studio Code IDE:
1. Install Visual Studio Code IDE.
2. Install Extensions: Dart & Flutter.

# Running Flutter Debugger

Once you have installed everything, you can run this command to start the debugger:

```
flutter run 
```

If you want to have a release version, you should use this command:

```
flutter run --release
```

If you want to run with detail, you should use this command:

```
flutter run --verbose
```

If you want to reset the pubspec.yml file, you should run this command:

```
flutter clean
flutter pub get
flutter pub run flutter_native_splash:create
flutter pub run flutter_launcher_icons:main
```


