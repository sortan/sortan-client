import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './constants.dart';

import './widgets/sortan_app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(
              bodyColor: kTextColor,
              displayColor: kTextColor,
            ),
        canvasColor: kPrimaryColor,
      ),
      home: SortanApp(),
    );
  }
}
