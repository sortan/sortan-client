import 'package:flutter/material.dart';

import '../constants.dart';

class HistoryPage extends StatelessWidget {
  const HistoryPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: kPrimaryColor,
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          elevation: 0,
          toolbarHeight: kAppBarSize,
          title: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              'Sortan',
              style: TextStyle(
                color: kAccentColor,
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
        ),
        body: Container(
          color: kPrimaryColor,
          child: Center(
            child: Text('History Page'),
          ),
        ),
      ),
    );
  }
}
