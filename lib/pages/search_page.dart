import 'package:flutter/material.dart';

import '../screens/search_screen.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SearchScreen(),
    );
  }
}