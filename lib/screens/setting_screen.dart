import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:open_file/open_file.dart';

import '../constants.dart';

import '../widgets/sortan_screen.dart';

import '../providers/result.dart';

import '../utils/utils.dart';

class SettingScreen extends StatelessWidget {
  final String recordedAudioPath;
  const SettingScreen({Key key, this.recordedAudioPath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _ipController =
        TextEditingController(); // Will delete later
    return SortanScreen(
      sortanScreenTitle: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        child: const Text(
          'Server IP Setting',
          style: TextStyle(
            color: kAccentColor,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      sortanScreenBody: Container(
        color: kPrimaryColor,
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //   mainAxisSize: MainAxisSize.max,
            //   children: [
            //     OutlinedButton.icon(
            //       onPressed: () {
            //         if (recordedAudioPath != null) {
            //           OpenFile.open(recordedAudioPath);
            //         } else {
            //           sortanToast("No currently recorded audio file");
            //         }
            //       },
            //       style: OutlinedButton.styleFrom(
            //         primary: Colors.white,
            //         side: BorderSide(
            //           color: kAccentColor,
            //         ),
            //       ),
            //       icon: const Icon(Icons.music_video),
            //       label: const Text("Recorded audio"),
            //     ),
            //     OutlinedButton.icon(
            //       onPressed: () async {
            //         await deleteCacheDir();
            //         await deleteAppDir();
            //       },
            //       style: OutlinedButton.styleFrom(
            //         primary: Colors.white,
            //         side: BorderSide(
            //           color: kAccentColor,
            //         ),
            //       ),
            //       icon: const Icon(Icons.delete_forever_outlined),
            //       label: const Text("Delete audios"),
            //     ),
            //   ],
            // ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  flex: 4,
                  child: Container(
                    child: TextField(
                      cursorColor: kAccentColor,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                        // hintText: "Server IP : Port Number",
                        hintText: Provider.of<Result>(context, listen: false)
                            .serverIp,
                        hintStyle: TextStyle(
                          color: Colors.white38,
                        ),
                        icon: Icon(
                          Icons.admin_panel_settings,
                          color: kAccentColor,
                          size: 40,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(kBorderRadius),
                            borderSide: BorderSide(
                              color: kAccentColor,
                            )),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(kBorderRadius),
                            borderSide: BorderSide(
                              color: Colors.white,
                            )),
                      ),
                      controller: _ipController,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: OutlinedButton(
                      child: const Text("Submit"),
                      style: OutlinedButton.styleFrom(
                        primary: kPrimaryColor,
                        backgroundColor: kAccentColor,
                      ),
                      onPressed: () {
                        Provider.of<Result>(context, listen: false)
                            .setIp(_ipController.text);
                        sortanToast("Server IP: ${_ipController.text}");
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
