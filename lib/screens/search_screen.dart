import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:path_provider/path_provider.dart' as sysPath;
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:app_settings/app_settings.dart';
import 'package:sortan/widgets/sortan_screen.dart';

import '../providers/result.dart';
import '../constants.dart';
import './result_screen.dart';
import './setting_screen.dart';

import '../widgets/circle_painter.dart';
import '../widgets/curve_wave.dart';
import '../widgets/sortan_button.dart';

import '../utils/utils.dart';

import '../exceptions/permission_exception.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with TickerProviderStateMixin {
  FlutterSoundRecorder _mRecorder = FlutterSoundRecorder();
  FlutterSoundHelper _mHelper = FlutterSoundHelper();
  bool _mRecorderIsInited = false;
  StreamSubscription _mRecorderSubscription;
  StreamSubscription _mRecordingDataSubscription;
  String _mPath;
  String recordedWav;
  Timer _timer;
  int _start;
  AnimationController _animationController;
  int _searchTimer = 10; // default record timer
  // Add duration completer
  // For N sec audio we add MS ms
  Map<int, int> durationCompleter = {
    5: 40,
    10: 60,
    15: 80,
    20: 60,
  };

  var sink;
  var recordingDataController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      duration: const Duration(
        milliseconds: 2000,
      ),
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _timer.cancel();

    stopRecorder();
    _mRecorder.closeAudioSession();
    _mRecorder = null;

    sink.close();
    recordingDataController.close();
    _mRecorderSubscription.cancel();

    _animationController.dispose();
    super.dispose();
  }

  Widget _buildSearchBtn() {
    return Center(
      child: ScaleTransition(
        scale: Tween(begin: 0.95, end: 1.0).animate(
          CurvedAnimation(
            parent: _animationController,
            curve: const CurveWave(),
          ),
        ),
        child: Text(
          !_mRecorder.isRecording ? 'Search' : _start.toString(),
          style: TextStyle(
            color: kPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
      ),
    );
  }

  void _startTimer() {
    _start = _searchTimer;
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  void _stopTimer() {
    setState(() {
      _start = 0;
    });
  }

  Future<void> _checkAndRequestPermission(BuildContext context) async {
    print(">>> Function : _checkAndRequestPermission()");
    try {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.microphone,
        Permission.storage,
      ].request();

      if (!statuses[Permission.microphone].isGranted) {
        if (statuses[Permission.microphone].isPermanentlyDenied ||
            statuses[Permission.storage].isPermanentlyDenied) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              backgroundColor: kPrimaryColor,
              title: Text("Permission Denied"),
              content: Text(
                  "Permission cannot be handled by Sortan. Click \"OK\" to open app setting."),
              actions: [
                SortanButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Close"),
                ),
                SortanButton(
                  onPressed: () async {
                    _mRecorder.closeAudioSession();
                    setState(() {
                      _mRecorderIsInited = false;
                    });
                    Navigator.of(context).pop();
                    AppSettings.openAppSettings();
                  },
                  child: Text("OK"),
                ),
              ],
            ),
          );
        } else {
          await _checkAndRequestPermission(context);
        }
      }
    } on PermissionException catch (e) {
      print('>>> Function : _checkAndRequestPermission() :: ${e.toString()}');
    }
    print(">>> Function : _checkAndRequestPermission() <DONE>");
  }

  Future<void> _openRecorder() async {
    print(">>> Function : _openRecorder()");
    await _checkAndRequestPermission(context).then((value) async {
      await _mRecorder.openAudioSession(
          focus: AudioFocus.requestFocusAndStopOthers);
      setState(() {
        _mRecorderIsInited = true;
      });
      print(">>> Function : _openRecorder() <DONE>");
    });
  }

  Future<IOSink> createFile() async {
    print(">>> Function : createFile()");
    var appDir = await sysPath.getExternalStorageDirectory();
    _mPath = '${appDir.path}/recorded_audio_${DateTime.now()}.pcm';
    var outputFile = File(_mPath);
    if (outputFile.existsSync()) {
      await outputFile.delete();
    }
    print(">>> Function : createFile() <DONE>");
    return outputFile.openWrite();
  }

  Future<void> recordAudio() async {
    print(">>> Function : recordAudio()");
    assert(_mRecorderIsInited);
    sink = await createFile();
    await _mRecorder.startRecorder(
      // toStream: recordingDataController.sink,
      toFile: _mPath,
      codec: Codec.pcm16,
      numChannels: 1,
      sampleRate: tSampleRate,
    );
    _startTimer();
    print(">>> Function : recordAudio() <DONE>");
  }

  Future<void> stopRecorder() async {
    print(">>> Function : stopRecorder()");
    await _mRecorder.stopRecorder().then((value) async {
      await _mRecorder.closeAudioSession().then((value) {
        setState(() {
          _mRecorderIsInited = false;
        });
      });
      _stopTimer();
      timeDilation = 1.0;
      // var appDir = await sysPath.getTemporaryDirectory();
      var appDir = await sysPath.getExternalStorageDirectory();
      recordedWav =
          '${appDir.path}/recorded_audio_${DateTime.now()}_${_searchTimer}s.wav';
      await _mHelper.pcmToWave(
        inputFile: _mPath,
        outputFile: recordedWav,
        numChannels: 1,
        sampleRate: tSampleRate,
      );
      // await deteleTempAudio(_mPath);
      print('>>>>> Path: $recordedWav');
    });
    if (_mRecordingDataSubscription != null) {
      await _mRecordingDataSubscription.cancel();
      _mRecordingDataSubscription = null;
    }
    print(">>> Function : stopRecorder() <DONE>");
  }

  Future<void> sortanAudio(BuildContext context) async {
    print(">>> Function : sortanAudio()");
    timeDilation = 0.50;
    print('Time delay: $timeDilation');
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.wifi ||
          connectivityResult == ConnectivityResult.mobile) {
        // Open recorder
        await _openRecorder().then((value) async {
          // Record audio
          await recordAudio();

          // Close recorder
          // Timer(Duration(milliseconds: kSearchTime), () async {  // Don't forget to change milliseconds back to kSearchTime
          Timer(
              Duration(
                  milliseconds: (_searchTimer * 1000) +
                      durationCompleter[_searchTimer]), () async {
            // Don't forget to change milliseconds back to kSearchTime
            await stopRecorder().then((value) async {
              var connectivityResult =
                  await (Connectivity().checkConnectivity());
              if (connectivityResult == ConnectivityResult.wifi ||
                  connectivityResult == ConnectivityResult.mobile) {
                Provider.of<Result>(context, listen: false)
                    .sendAudio(recordedWav);
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (ctx) => ResultScreen(),
                  ),
                );
              } else {
                timeDilation = 1.0;
                sortanToast("No internet connection");
              }
            });
          });
          setState(() {});
          print(">>> Function : sortanAudio() <DONE>");
        });
      } else {
        timeDilation = 1.0;
        sortanToast("No internet connection");
      }
    } on PermissionException catch (e) {
      e.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SortanScreen(
      sortanOnWillPop: () async {
        if (_mRecorder.isRecording) {
          sortanToast("Cannot use Back button while recording");
          return false;
        }
        return true;
      },
      sortanScreenTitle: Padding(
        padding: const EdgeInsets.all(12.0),
        child: GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (ctx) => SettingScreen(
                  recordedAudioPath: recordedWav,
                ),
              ),
            );
          },
          child: Text(
            'Sortan',
            style: TextStyle(
              color: kAccentColor,
              fontWeight: FontWeight.bold,
              fontSize: 30,
            ),
          ),
        ),
      ),
      sortanScreenAction: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: GestureDetector(
            onTap: () {
              if (!_mRecorder.isRecording) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (ctx) => ResultScreen(),
                  ),
                );
              } else {
                sortanToast("The app is listening");
              }
            },
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(kBorderRadius),
                  color: kAccentColor,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Result',
                    style: TextStyle(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
      sortanScreenBody: Container(
        color: kPrimaryColor,
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomPaint(
              painter: CirclePainter(
                _animationController,
                color: kAccentColor,
              ),
              child: SizedBox(
                width: kSearchBtnSize * 3.125,
                height: kSearchBtnSize * 3.125,
                child: GestureDetector(
                  onTap: _mRecorder.isRecording
                      ? () => {}
                      : () => {
                            sortanAudio(context),
                          },
                  child: _buildSearchBtn(),
                ),
              ),
            ),
            DropdownButton(
              value: _searchTimer,
              items: [
                DropdownMenuItem(
                  child: Text("5s"),
                  value: 5,
                ),
                DropdownMenuItem(
                  child: Text("10s"),
                  value: 10,
                ),
                // DropdownMenuItem(
                //   child: Text("15s"),
                //   value: 15,
                // ),
                // DropdownMenuItem(
                //   child: Text("20s"),
                //   value: 20,
                // ),
              ],
              onChanged: (value) {
                setState(() {
                  _searchTimer = value;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
