import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants.dart';

import '../widgets/sortan_screen.dart';

import '../providers/result.dart';

import '../utils/utils.dart';

void _openYoutubeVideo(String url) async {
  await canLaunch(url) ? await launch(url) : sortanToast("Cannot open $url");
}

class ResultScreen extends StatefulWidget {
  @override
  _ResultScreenState createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  @override
  Widget build(BuildContext context) {
    timeDilation = 1.0;
    double screenWidth = MediaQuery.of(context).size.width;
    double thumbnailWidth = screenWidth - 175;

    return Consumer<Result>(
      builder: (ctx, result, _) => !result.isHasReponse
          ? SortanScreen(
              sortanScreenTitle: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  'Result',
                  style: TextStyle(
                    color: kAccentColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
              sortanScreenBody: result.isGettingResponse
                  ? Container(
                      // Loading screen
                      color: kPrimaryColor,
                      constraints: BoxConstraints.expand(),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Getting result from the server..."),
                          SizedBox(
                            height: 5,
                          ),
                          SizedBox(
                            width: screenWidth * 0.125,
                            height: screenWidth * 0.125,
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(kAccentColor),
                            ),
                          )
                        ],
                      ),
                    )
                  : Container(
                      // Screen that user have not sortaned any song yet
                      color: kPrimaryColor,
                      constraints: BoxConstraints.expand(),
                      child: const Center(
                        child: const Text("You haven't sortaned any song yet"),
                      ),
                    ),
            )
          : SortanScreen(
              sortanScreenTitle: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: const Text(
                  'Result',
                  style: TextStyle(
                    color: kAccentColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
              sortanScreenBody: result.isErrorOccur
                  ? Container(
                      // Screen with error message
                      color: kPrimaryColor,
                      constraints: BoxConstraints.expand(),
                      child: Center(
                        child: Text(result.errorMsg),
                      ),
                    )
                  : Container(
                      // main screen
                      color: kPrimaryColor,
                      constraints: BoxConstraints.expand(),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 50,
                            child: Text(
                              result.songTitle,
                              style: TextStyle(fontSize: 24),
                            ),
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(16.0),
                            child: Container(
                              color: Colors.white,
                              child: Image.network(
                                getYoutubeThumbnailLink(result.songYoutubeUrl),
                                width: thumbnailWidth,
                                height: thumbnailWidth,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                SizedBox(
                                  height: 30,
                                  child: Text(
                                    result.songAuthor,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Other actions',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      String url = result.songYoutubeUrl;
                                      _openYoutubeVideo(url);
                                    },
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Image.asset(
                                            'assets/images/youtube_logo.png',
                                            width: 100,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Text(
                                            'Click to view music on Youtube',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: Image.asset(
                                              'assets/images/sortan-logo.png',
                                              width: 50,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Text(
                                            'Sortan another music',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
            ),
    );
  }
}
