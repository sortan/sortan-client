import 'package:flutter/material.dart';
import 'package:sortan/widgets/sortan_app.dart';

import './tab_item.dart';
import '../constants.dart';

class SortanNavBar extends StatelessWidget {
  final ValueChanged<int> onSelectTab;
  final List<TabItem> tabs;

  SortanNavBar({
    @required this.onSelectTab,
    @required this.tabs,
  });

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: tabs
          .map(
            (e) => _buildItem(
              index: e.getIndex(),
              icon: e.icon,
              tabName: e.tabName,
            ),
          )
          .toList(),
      onTap: (index) => onSelectTab(
        index,
      ),
      backgroundColor: kPrimaryColor,
    );
  }

  BottomNavigationBarItem _buildItem(
      {int index, IconData icon, String tabName}) {
    return BottomNavigationBarItem(
      icon: Icon(
        icon,
        color: _tabColor(index),
        size: 35,      
      ),
      title: Text(
        tabName,
        style: TextStyle(
          color: _tabColor(index),
          fontSize: 16,
        ),
      ),
    );
  }

  Color _tabColor(int index) {
    return SortanAppState.currentTabIndex == index ? kAccentColor : kTextColor;
  }
}
