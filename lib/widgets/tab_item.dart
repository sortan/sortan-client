import 'package:flutter/material.dart';
import './sortan_app.dart';

class TabItem {
  final String tabName;
  final IconData icon;
  final GlobalKey<NavigatorState> key = GlobalKey<NavigatorState>();
  int _index = 0;
  Widget _page;

  TabItem({
    @required this.tabName,
    @required this.icon,
    @required Widget page,
  }) {
    _page = page;
  }

  void setIndex(int index) {
    _index = index;
  }

  int getIndex() => _index;

  Widget get page {
    return Visibility(
      visible: _index == SortanAppState.currentTabIndex,
      // important to preserve state while switching between tabs
      maintainState: true,
      child: Navigator(
        // key tracks state change
        key: key,
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            builder: (_) => _page,
          );
        },
      ),
    );
  }
}
