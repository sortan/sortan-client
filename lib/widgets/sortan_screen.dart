import 'package:flutter/material.dart';

import '../constants.dart';

class SortanScreen extends StatelessWidget {
  final Widget sortanScreenTitle;
  final Widget sortanScreenBody;
  final List<Widget> sortanScreenAction;
  final Function sortanOnWillPop;

  const SortanScreen({
    Key key,
    @required this.sortanScreenTitle,
    this.sortanScreenAction,
    this.sortanOnWillPop,
    @required this.sortanScreenBody,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: sortanOnWillPop,
        child: Scaffold(
            backgroundColor: kPrimaryColor,
            appBar: AppBar(
              backgroundColor: kPrimaryColor,
              elevation: 0,
              toolbarHeight: kAppBarSize,
              title: sortanScreenTitle,
              actions: sortanScreenAction,
            ),
            body: sortanScreenBody),
      ),
    );
  }
}
