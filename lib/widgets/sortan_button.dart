import 'package:flutter/material.dart';

import '../constants.dart';

class SortanButton extends StatelessWidget {
  final Widget child;
  final Function onPressed;
  
  SortanButton({
    Key key,
    @required this.child,
    @required this.onPressed,
  }) : super(key: key);

  final ButtonStyle _raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: kPrimaryColor,
    primary: kAccentColor,
    animationDuration: const Duration(milliseconds: 100),
    minimumSize: const Size(100, 36),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(kBorderRadius)),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: _raisedButtonStyle,
      child: child,
      onPressed: onPressed,
    );
  }
}
