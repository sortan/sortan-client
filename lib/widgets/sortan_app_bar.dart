import 'package:flutter/material.dart';

import '../constants.dart';

class SortanAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;

  SortanAppBar({Key key}) : preferredSize = Size.fromHeight(kAppBarSize), super(key: key);
  
  @override
  _SortanAppBarState createState() => _SortanAppBarState();
}

class _SortanAppBarState extends State<SortanAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Text('Sortan')],
      ),
    );
  }
}
