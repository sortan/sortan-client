import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../constants.dart';
import '../providers/result.dart';

import './sortan_nav_bar.dart';
import './tab_item.dart';
import './animated_indexed_stack.dart';

import '../pages/search_page.dart';
import '../pages/history_page.dart';

class SortanApp extends StatefulWidget {
  SortanApp({Key key}) : super(key: key);

  @override
  SortanAppState createState() => SortanAppState();
}

class SortanAppState extends State<SortanApp> {
  static int currentTabIndex = 0;
  
  final List<TabItem> tabs = [
    TabItem(
      tabName: "Search",
      icon: Icons.search,
      page: SearchPage(),
    ),
    TabItem(
      tabName: "History",
      icon: Icons.list,
      page: HistoryPage(),
    ),
  ];

  SortanAppState() {
    // indexing is necessary for proper funcationality of determining which tab is active
    tabs.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  // sets current tab index and update state
  void _selectTab(int index) {
    if (index == currentTabIndex) {
      // pop to first route
      // if the user taps on the active tab
      tabs[index].key.currentState.popUntil((route) => route.isFirst);
    } else {
      // update the state
      // in order to repaint
      setState(() => currentTabIndex = index);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => Result(),
        ),
      ],
      child: WillPopScope(
        onWillPop: () async {
          final isFirstRouteInCurrentTab =
              !await tabs[currentTabIndex].key.currentState.maybePop();
          if (isFirstRouteInCurrentTab) {
            // if not on the 'main' tab
            if (currentTabIndex != 0) {
              // select the main tab
              _selectTab(0);
              // back button handled by app
              return false;
            }
          }
          // let system handle back button if we're on the first route
          return isFirstRouteInCurrentTab;
        },
        // this is the base scaffold
        // don't put appbar in here otherwise you might end up
        // with multiple appbars on one screen
        // eventually breaking the app
        child: Scaffold(
          backgroundColor: kPrimaryColor,
          body: AnimatedIndexedStack(
            index: currentTabIndex,
            children: tabs.map((e) => e.page).toList(),
          ),
          bottomNavigationBar: SortanNavBar(
            tabs: tabs,
            onSelectTab: _selectTab,
          ),
        ),
      ),
    );
  }
}
