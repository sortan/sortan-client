class PermissionException implements Exception {
  String _msg;

  PermissionException([String msg = 'Permission denied.']) {
    this._msg = msg;
  }

  @override
  String toString() {
    return _msg;
  }
}