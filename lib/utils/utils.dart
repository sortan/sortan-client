import 'dart:io';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart' as sysPath;

import '../constants.dart';

Future<void> sortanToast(String msg) async {
  return await Fluttertoast.showToast(
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 1,
    backgroundColor: kAccentColor,
    textColor: kPrimaryColor,
    msg: msg,
  );
}

Future<void> deteleTempAudio(String filePath) async {
  print('>>> Function : deteleTempAudio() <$filePath>');
  File file = File(filePath);
  try {
    if (await file.exists()) {
      await file.delete().then((_) {
        print('>>> Function : deteleTempAudio() <DONE>');
      });
    }
  } catch (e) {
    throw e;
  }
}

Future<String> getLocalIpAddress() async {
  final interfaces = await NetworkInterface.list(
      type: InternetAddressType.IPv4, includeLinkLocal: true);

  try {
    // Try VPN connection first
    NetworkInterface vpnInterface =
        interfaces.firstWhere((element) => element.name == "tun0");
    return vpnInterface.addresses.first.address;
  } on StateError {
    // Try wlan connection next
    try {
      NetworkInterface interface =
          interfaces.firstWhere((element) => element.name == "wlan0");
      return interface.addresses.first.address;
    } catch (ex) {
      // Try any other connection next
      try {
        NetworkInterface interface = interfaces.firstWhere(
            (element) => !(element.name == "tun0" || element.name == "wlan0"));
        return interface.addresses.first.address;
      } catch (ex) {
        return null;
      }
    }
  }
}

Future<void> deleteAppDir() async {
  print('>>> Function : _deleteAppDir()');
  final appDir = await sysPath.getExternalStorageDirectory();
  print(">>> appDir :: $appDir");
  appDir.deleteSync(recursive: true);

  print('>>> Function : _deleteAppDir() <DONE>');
}

String getYoutubeThumbnailLink(String videoUrl) {
  final Uri uri = Uri.tryParse(videoUrl);
  if (uri == null) {
    return null;
  }
  print(
      '>>> result.dart --> getYoutubeThumbnailLink ::: https://img.youtube.com/vi/${uri.queryParameters['v']}/0.jpg');
  return 'https://img.youtube.com/vi/${uri.queryParameters['v']}/0.jpg';
}
