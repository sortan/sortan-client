import 'package:flutter/material.dart';

const kTextColor = Color(0xFFFFFFFF);
const kPrimaryColor = Color(0xFF1C2529);
const kAccentColor = Color(0xFF02E494);

const kDefaultPadding = 20.0;
const kBorderRadius = 7.0;

const baseUrl = "https://api.sortan-core.com";

// Search Button
const kSearchBtnSize = 50.0;

// Font
const kAppBarSize = 100.0;

const int tSampleRate = 44100;
const int tBitDepth = 16;

const int kTimer = 10;
const int kSearchTime = kTimer * 1000;
