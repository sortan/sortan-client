import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:dio/dio.dart';
import 'package:sortan/constants.dart';
import 'package:vibration/vibration.dart';

import '../utils/utils.dart';

class Result extends ChangeNotifier {
  String resultId;
  int songId;
  String songTitle;
  String songImageUrl;
  String songAuthor;
  String songYoutubeUrl;
  bool isGettingResponse;
  bool isHasReponse;
  bool isErrorOccur;
  String errorMsg;
  String serverIp;

  Result({
    this.resultId,
    this.songId,
    this.songTitle,
    this.songImageUrl,
    this.songAuthor,
    this.songYoutubeUrl,
    this.isGettingResponse = false,
    this.isHasReponse = false,
    this.isErrorOccur = false,
    this.serverIp = baseUrl,
  });

  Future<void> sendAudio(String audioPath) async {
    print('>>> Function : sendAudio()');
    var dio = Dio();
    dio.options.connectTimeout = 120 * 1000;
    dio.options.receiveTimeout = 120 * 1000;
    isGettingResponse = true;
    if (isHasReponse) {
      isHasReponse = !isHasReponse;
      notifyListeners();
    }

    final String url = "$serverIp/musics/search";
    CancelToken token = CancelToken();

    final fileName = basename(audioPath);
    print('>>> URL: $url | FILE: $fileName');

    try {
      FormData formData = new FormData.fromMap({
        'file': await MultipartFile.fromFile(audioPath, filename: fileName),
      });

      await dio.post(url, cancelToken: token, data: formData,
          onSendProgress: (int sent, int total) {
        print(">>> onSendProgress: \t\t $sent / $total");
      }, onReceiveProgress: (int received, int total) {
        print(">>> onReceivedProgress: \t $received / $total");
      }).then(
        (response) {
          print(">>>>> Response from server: $response");
          resultId = DateTime.now().toString();
          Map responseBody = response.data;
          songId = responseBody["message"]["id"];
          songTitle = responseBody["message"]["name"].toString();
          songYoutubeUrl =
              responseBody["message"]["links"][0]["url"].toString();
          songImageUrl = getYoutubeThumbnailLink(songYoutubeUrl);
          songAuthor = responseBody["message"]["artists"][0]["name"].toString();
          isHasReponse = true;
          isErrorOccur = false;
          notifyListeners();
        },
      ).catchError(
        (e) {
          print(">>>>> Error from server -- Data : ${e.response.data}");
          print(">>>>> Error from server -- headers: ${e.response.headers}");
          print(">>>>> Error from server -- error type: ${e.type}");
          print(
              ">>>>> Error from server -- error code: ${e.response.statusCode}");
          if (e.response.statusCode == 500) {
            this.errorMsg = "500 Internal Server Error";
          } else if (e.response.statusCode == 502) {
            this.errorMsg = "502 Bad Gateway";
          } else if (e.response.statusCode == 503) {
            this.errorMsg = "503 Service Unavailable";
          } else if (e.response.statusCode == 504) {
            this.errorMsg = "504 Gateway Timeout";
          } else if (e.response.statusCode == 507) {
            this.errorMsg = "507 Insufficient Storage";
          } else if (e.response.statusCode == 400) {
            this.errorMsg = "No music found";
          } else {
            this.errorMsg = "Error code: ${e.response.statusCode}";
          }
          this.isHasReponse = true;
          this.isErrorOccur = true;
          notifyListeners();
        },
      );
    } catch (error) {
      print('[result]: $error');
      throw error;
    } finally {
      isGettingResponse = false;
      token.cancel('cancel');
      await deleteAppDir();
      await _vibrateResult();
    }
    print('>>> Function : sendAudio() <DONE>');
  }

  void setIp(String ipAddr) {
    this.serverIp = ipAddr;
    print(">>> Server IP Address: $ipAddr");
  }

  Future<void> _vibrateResult() async {
    print('>>> Function : _vibrateResult()');
    if (await Vibration.hasVibrator()) {
      Vibration.vibrate(duration: 1000);
    }
    print('>>> Function : _vibrateResult() <DONE>');
  }
}
